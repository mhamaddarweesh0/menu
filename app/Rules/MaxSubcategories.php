<?php

namespace App\Rules;

use App\Models\Category;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Http\Exceptions\HttpResponseException;

class MaxSubcategories implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        //
        $category = Category::find($value);

        if (!$category) {
            $fail('Not Found');
        }
        if(!$category->checkMaximumLevelValidity())
            $fail($this->message());
    }



    public function message()
    {
        return 'The category tree cannot have more than four levels of subcategories.';
    }

}
