<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'category_id'
    ];


    protected $appends = ['discounted_price'];

    public function getDiscountedPriceAttribute()
    {
        // Your logic to compute discounted price goes here
        return $this->getDiscountValue();
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }


    public function discounts()
    {
        return $this->belongsToMany(Discount::class, 'item_discounts');
    }


    public function discount()
    {
        return $this->discounts->first();
    }

    public function getDiscountValue()
    {
        if (!$this->discount()){
            $discount = $this->category->discounted_price;
        }else{
            $discount = $this->discount()->discount_value;
        }
        return $discount;
    }

    public function getPriceAttribute($value){
        return round(((100-$this->discounted_price)/100)*$value, 2);
    }

    public static function addNew($item_data){
        $item['name'] = $item_data['name'];
        $item['category_id'] = $item_data['category_id'];
        $item['price'] = $item_data['price'];
        self::create($item);
    }
}
