<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    const MaxAlowedNumberOfSubCategories = 4;
    protected $fillable = [
        'name',
        'parent_id'
    ];

    protected $appends = ['discounted_price'];

    public function getDiscountedPriceAttribute()
    {
        // Your logic to compute discounted price goes here
        return $this->getDiscountValue();
    }

    public static function getCategory($category_id){
        $does_category_children_exists = Category::where('parent_id', $category_id)->exists();
        if ($does_category_children_exists)
            $category = Category::with(['children.category', 'category'])->find($category_id);
        else
            $category = Category::with(['items'])->find($category_id);
        return $category;
    }

    public static function getRootCategoryNodes(){
        return Category::whereNull('parent_id')->get();
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function hasItems()
    {
        return $this->items->count() > 0;
    }

    public function hasChildren()
    {
        return $this->children->count() > 0;
    }

    public function getLevel()
    {
        $level = 0;
        $currentCategory = $this;

        while ($currentCategory->category) {
            $level++;
            $currentCategory = $currentCategory->category;
        }
        return $level;
    }

    public function checkMaximumLevelValidity()
    {
        return $this->getLevel() < self::MaxAlowedNumberOfSubCategories;
    }

    public function discounts()
    {
        return $this->belongsToMany(Discount::class, 'category_discounts');
    }

    public function discount()
    {
        return $this->discounts->first();
    }

    public function getDiscountValue()
    {
        $discount = 0;
        $currentCategory = $this;
        while ($currentCategory) {
            if ($currentCategory->discount()) {
                $discount = $currentCategory->discount()->discount_value;
                break;
            }
            $currentCategory = $currentCategory->category;
        }

        if ($discount == 0)
        {
            $discount = Discount::getAllCategoryDiscountValue();
        }
        return $discount;
    }

    public static function addNew($category_data){
        return self::create([
            'name'=>$category_data['name'],
            'parent_id'=>$category_data['parent_id']
        ]);
    }
}
