<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemDiscount extends Model
{
    use HasFactory;

    protected $fillable = [
        'item_id',
        'discount_id'
    ];

    public static function insertMultiple($item_ids, $discount_id){
        $discount_items = array_map(function ($item) use ($discount_id){
            return [
                'discount_id'=>$discount_id,
                'item_id'=>$item
            ];
        }, $item_ids);
        ItemDiscount::insert($discount_items);
    }
}
