<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryDiscount extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'discount_id'
    ];

    public static function insertMultiple($category_ids, $discount_id){
        $discount_category = array_map(function ($category) use ($discount_id){
            return [
                'discount_id'=>$discount_id,
                'category_id'=>$category
            ];
        }, $category_ids);
        CategoryDiscount::insert($discount_category);
    }
}
