<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
        'discount_value',
        'discount_type'
    ];

    public static function getAllCategoryDiscountValue(){
        $discount = self::where('discount_type', 'all')->first();
        if ($discount)
            return $discount->discount_value;
        return 0;
    }
}
