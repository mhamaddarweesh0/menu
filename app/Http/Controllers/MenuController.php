<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Services\MenuService;
use App\Models\Category;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MenuController  extends Controller
{
    //
    public function index(Request $request){
        $data = MenuService::getMenu($request->input('category_id'));
        return response()->json($data);
    }
}
