<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Services\DiscountService;
use App\Models\Category;
use App\Models\CategoryDiscount;
use App\Models\Discount;
use App\Models\Item;
use App\Models\ItemDiscount;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DiscountController extends Controller
{
    //
    public function index(){
        $discounts = Discount::get();
        return response()->json(['discounts'=>$discounts]);
    }
    //
    public function store(Request $request){
        try {
            return DiscountService::createDiscount($request->all());
        }catch (\Exception $exception){
            return response()->json([
                'success'=>false,
                'message'=>$exception->getMessage(),
            ]);
        }
    }
}
