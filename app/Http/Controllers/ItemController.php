<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateItemRequest;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ItemController extends Controller
{
    //
    public function index(){
        $items = Item::with(['category'])->get();
        return response()->json(['items'=>$items]);
    }
    //
    public function store(CreateItemRequest $request){
        try {
            Item::addNew($request->all());
            return response()->json([
                'success'=>true,
                'message'=>'',
            ]);
        }catch (\Exception $exception){
            return response()->json([
                'success'=>false,
                'message'=>$exception->getMessage(),
            ]);
        }
    }
}
