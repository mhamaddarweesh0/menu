<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{
    //
    public function index(){
        $categories = Category::with(['category'])->get();
        return response()->json(['categories'=>$categories]);
    }
    //
    public function store(CreateCategoryRequest $request){
        try {
            Category::addNew($request->all());
            return response()->json([
                'success'=>true,
                'message'=>'Category Created Successfully',
            ]);
        }catch (\Exception $exception){
            return response()->json([
                'success'=>false,
                'message'=>$exception->getMessage(),
            ]);
        }
    }
}
