<?php


namespace App\Http\Services;


use App\Models\Category;

class MenuService
{
 public static function getMenu($category_id){
     if ($category_id) {
        $data['category'] = Category::getCategory($category_id);
     }
     else
         $data['categories'] = Category::getRootCategoryNodes();
     return $data;
 }
}
