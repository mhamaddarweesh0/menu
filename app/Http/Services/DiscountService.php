<?php


namespace App\Http\Services;


use App\Models\CategoryDiscount;
use App\Models\Discount;
use App\Models\ItemDiscount;

class DiscountService
{
    public static function createDiscount($discount_data){
        $discount_object = [
            'discount_value'=>$discount_data['discount_value'],
            'discount_type'=>$discount_data['discount_type'],
        ];
        $discount = Discount::create($discount_object);
        switch ($discount_data['discount_type']){
            case 'category':
                CategoryDiscount::insertMultiple($discount_data['category_ids'], $discount->id);
                break;
            case 'item':
                ItemDiscount::insertMultiple($discount_data['item_ids'], $discount->id);
                break;
        }

        return response()->json([
            'success'=>true,
            'message'=>'',
        ]);
    }
}
