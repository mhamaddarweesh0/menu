<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/categories', [\App\Http\Controllers\CategoryController::class, 'index']);
Route::post('/categories', [\App\Http\Controllers\CategoryController::class, 'store']);

Route::get('/items', [\App\Http\Controllers\ItemController::class, 'index']);
Route::post('/items', [\App\Http\Controllers\ItemController::class, 'store']);

Route::get('/discounts', [\App\Http\Controllers\DiscountController::class, 'index']);
Route::post('/discounts', [\App\Http\Controllers\DiscountController::class, 'store']);

Route::get('/menu_items', [\App\Http\Controllers\MenuController::class, 'index']);
