import { createRouter, createWebHistory} from "vue-router";
import categoryIndex from '../components/category/category_index.vue'
import createCategory from '../components/category/category_create.vue'
import itemIndex from '../components/item/item_index.vue'
import createItem from '../components/item/item_create.vue'
import discountIndex from '../components/discount/discount_index.vue'
import createDiscount from '../components/discount/discount_create.vue'
import menuIndex from '../components/menu/menu_items.vue'
import notFound from '../components/not_found.vue'

const routes = [
    {
        path: '/',
        component: menuIndex
    },
    {
        path: '/menu',
        component: menuIndex
    },
    {
        path: '/category',
        component: categoryIndex
    },
    {
        path: '/create_category',
        component: createCategory
    },
    {
        path: '/item',
        component: itemIndex
    },
    {
        path: '/create_item',
        component: createItem
    },
    {
        path: '/discount',
        component: discountIndex
    },
    {
        path: '/create_discount',
        component: createDiscount
    },
    {
        path: '/:pathMatch(.*)*',
        component: notFound
    }
]



const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router;
